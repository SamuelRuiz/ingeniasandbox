#include "StdAfx.h"
#include "MovilBlando.h"


MovilBlando::MovilBlando(void)
	: Movil(3.0, 4.0)
{
}


MovilBlando::~MovilBlando(void)
{
}

void MovilBlando::simular(double inc_t) 
{
	Movil::simular(inc_t);
	x = x - inc_t;
}
