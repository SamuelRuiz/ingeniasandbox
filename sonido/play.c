
/* 
ATENCION!!!!
No está bien. Falta algún detalle que hace que no funcione
*/

static Uint8 *audio_chunk;
static Uint32 audio_len;
static Uint8 *audio_pos;

/* The audio function callback takes the following parameters:
   stream:  A pointer to the audio buffer to be filled
   len:     The length (in bytes) of the audio buffer
*/
void fill_audio(void *udata, Uint8 *stream, int len)
{
    SDL_AudioSpec *p = (SDL_AudioSpec *)udata;
    
    unsigned t =  time(0);
    printf("tiempo: %u\n", t );
    
    printf("fill_audio, len= %i\n", len);
    printf("fill_audio, audio_len= %u\n", audio_len);
        
    SDL_memset(stream, 0, len);
    
    /* Only play if we have data left */
    if ( audio_len == 0 )
        return;

    /* Mix as much data as possible */
    len = ( len > audio_len ? audio_len : len );
    SDL_MixAudioFormat(stream, audio_pos, p->format, len, SDL_MIX_MAXVOLUME);
    audio_pos += len;
    audio_len -= len;
}

int main(int argc, char** argv) 
{
    SDL_AudioSpec want, have;
    SDL_AudioDeviceID dev;
    int i, count;

    if ( SDL_Init(SDL_INIT_AUDIO) < 0 )
        return 1;
    
    count = SDL_GetNumAudioDevices(0);

    for (i = 0; i < count; ++i) {
        SDL_Log("Audio device %d: %s", i, SDL_GetAudioDeviceName(i, 0));
    }
    
    /* Set the audio format */
    /*wanted.freq = 22050; 
    wanted.format = AUDIO_S16; 
    wanted.channels = 2; */   /* 1 = mono, 2 = stereo */
    /* wanted.samples = 1024; Good low-latency value for callback 
    wanted.samples = 1024; 
    wanted.callback = fill_audio;
    wanted.userdata = &wanted; */
    /* Open the audio device, forcing the desired format */
    /*if ( SDL_OpenAudio(&wanted, NULL) < 0 ) { 
            fprintf(stderr, "Couldn't open audio: %s\n", SDL_GetError());
        return(-1);
    }
    */ 

    SDL_memset(&want, 0, sizeof(want)); /* or SDL_zero(want) */
    want.freq = 48000;
    want.format = AUDIO_S16; /* AUDIO_F32; */
    want.channels = 2;
    want.samples = 4096;
    want.callback = fill_audio;  
    want.userdata = &want;

    dev = SDL_OpenAudioDevice(NULL, 0, &want, &have, SDL_AUDIO_ALLOW_FORMAT_CHANGE);
    if (dev == 0) {
        printf("Failed to open audio: %s\n", SDL_GetError());
        SDL_CloseAudio();
        return 1;
    } 
    
    if (have.format != want.format) { 
        printf("We didn't get Float32 audio format.\n");
    }

    /* Load the audio data ... */
    if ( SDL_LoadWAV(argv[1], &have, &audio_chunk, &audio_len) == NULL ) 
    {
        SDL_CloseAudio();
        printf("No he podido cargar el archivo\n");
        return 1;
    }
    
    printf("Channels: %u\n", have.channels);
    printf("Format: %u (de entrada es %u)\n", have.format, AUDIO_S16);

    audio_pos = audio_chunk;

    SDL_PauseAudioDevice(dev, 0); /* start audio playing. */

    /* Let the callback function play the audio chunk */
    /*SDL_PauseAudio(0); */

    /* Do some processing */

    /* Wait for sound to complete */
    /* while ( audio_len > 0 ) { */
        /*SDL_Delay(10000);*/         /* Sleep 20000 ms */
    /*}*/
    /*SDL_CloseAudio();*/
    scanf("%*c");
    SDL_CloseAudioDevice(dev);
    SDL_FreeWAV(audio_chunk);
    return 0;
}
