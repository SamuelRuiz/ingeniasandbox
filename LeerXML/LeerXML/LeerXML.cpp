// LeerXML.cpp: define el punto de entrada de la aplicación de consola.
//

#include "stdafx.h"



int main()
{
	FILE * pFile;
	long lSize;
	char * buffer;
	size_t result;

	pFile = fopen ( "Datos.xml" , "rb" );
	if (pFile==NULL) {fputs ("File error",stderr); exit (1);}

	// obtain file size:
	fseek (pFile , 0 , SEEK_END);
	lSize = ftell (pFile);
	rewind (pFile);

	// allocate memory to contain the whole file:
	buffer = (char*) malloc (sizeof(char)*(lSize+1));
	if (buffer == NULL) {fputs ("Memory error",stderr); exit (2);}

	// copy the file into the buffer:
	result = fread (buffer,1,lSize,pFile);
	if (result != lSize) {fputs ("Reading error",stderr); exit (3);}
	buffer[lSize]='\0';

	cout << buffer << endl << endl;

	xml_document<> doc;
	doc.parse<0>(buffer);


	xml_node<> *node = doc.first_node();
	cout << "Name of my first node is: " << node->name() << "\n";
	xml_node<> *node2 = node->first_node(); //tree
	cout << "Name: " << node2->name() << "\n";
	xml_node<> *node3 = node->first_node()->first_node(); //posicion
	cout << "Name: " << node3->name() << "\n";
	xml_node<> *node4 = node3->first_node("Y"); //Y
	cout << "Value: " << node4->value() << "\n";

	system("pause");

	/* the whole file is now loaded in the memory buffer. */

	// terminate
	fclose (pFile);
	free (buffer);
	return 0;
}

